# Pig Dice

App for playing the Pig Dice game:

- <https://en.wikipedia.org/wiki/Pig_(dice_game)>
- <https://rosettacode.org/wiki/Pig_the_dice_game>

This app can be used for learning Flutter.

The whole app is implemented in a single file: <lib/main.dart>

## Steps for creating the App

Here are the steps I went through to create the app:

- Create a two columns layout, with a `ListView` of players (strings) on the left and  input field With add button on the right (use `Expanded` to get different column widths).
- Use a list of strings for the list of players.
- Add a controller to the input field and let the _add_ button to add the new name.
- Add a way to remove a player (swiping it out...)
- Add a  _start_ button in the right column.
- Add a `_playing` state variable that can be switched with the _start_ button
- Use the `playing` variable to switch the content of the right column between the input / start and a new _roll_ button.
- Use a `Player` class for the `_players` list. It should have the fields `name` and `score`
- When playing, show the current player name and score in the right column (it's the first player in the list...)
- Roll the dice and show the value in the right column
- Add a state variable for the round score, show it in the right column, and use it sum the dice value after each rolling.
- Clicking on _Next Player_:
   - adds the round score to the current player,
   - resets the round score to 0, and
   - resets the dice score to null (or any value that will not be shown).
- Disable _Next Player_ before the player has rolled the dice once.
- On _Next Player_ move the top player to the bottom of the list.
- If the dice value is 1, set the round score to 0 and disable the _Roll_ button.
- When _Start_ is pressed correctly initialize the state variable and reset the players' score to 0.
- Finish the game if the current player gets to 100 points: instead of `_playing` we might use a `_gameState` enum with the three states _config_, _play_, _win_.

The result of those steps are in the first commit.

In further commits, I will improve the look of the app and polish it a bit.

## Things to learn

In this section, there will be a summary of the Dart and Flutter snippets that are useful to create this app.

### Rendering a column of widgets

Use `Expanded` to define the width the of the columns:

### Rendering a row of widgets

### Buttons with actions

Disable the button:

### Setting the state

### Text input fiels

### Conditionally render widgets