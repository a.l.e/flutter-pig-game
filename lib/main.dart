import 'package:flutter/material.dart';
import 'dart:math';

enum GameState {
  config,
  play,
  win,
}

class Player {
  final String name;
  int score = 0;
  Color color;
  Player (this.name, this.color);
}

// #ee4035 • #f37736 • #fdf498 • #7bc043 • #0392cf
const playerColor = [
  Color(0xfffe8a71),
  Color(0xff2ab7ca),
  Color(0xff7bc043),
  Color(0xff4a4e4d),
  Color(0xfff6cd61),
];

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.teal,
      ),
      home: const MyHomePage(title: 'Pig Dice'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final _targetScore = 100;

  late TextEditingController _playerNameTextFieldController;
  final _randomGenerator = Random();

  // state variables
  bool _isPlayerNameFilled = false;
  GameState _gameState = GameState.config;
  int? _diceScore;
  int _roundScore = 0;
  final List<Player> _players = [];


  @override
  void initState() {
    super.initState();
    _playerNameTextFieldController =  TextEditingController();
    _playerNameTextFieldController.addListener(() {
      setState(() => _isPlayerNameFilled = _playerNameTextFieldController.text.isNotEmpty);
    });
  }

  @override
  void dispose() {
    _playerNameTextFieldController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body:
        Padding(
          padding: const EdgeInsets.all(20),
          child: _gameState == GameState.config || _gameState == GameState.win?
            Column(
              children: [
                SizedBox(
                  width: MediaQuery.of(context).size.width * 0.15,
                  height: MediaQuery.of(context).size.width * 0.15,
                  child: ElevatedButton(
                    onPressed: _players.isEmpty ? null : () => setState(() {
                      _players.shuffle();
                      _gameState = GameState.play;
                      _initGame();
                    }),
                    style: ElevatedButton.styleFrom(
                      // shape: const CircleBorder(),
                      padding: const EdgeInsets.all(20),
                      backgroundColor: Theme.of(context).colorScheme.background,
                      foregroundColor: Theme.of(context).colorScheme.outline, // Splash color
                    ),
                    child: const FittedBox(
                      fit: BoxFit.contain,
                      child:Text('Play', style: TextStyle(color: Colors.white)),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                if (_gameState == GameState.win)
                  Text(
                    '${_players.first.name} won with ${_players.first.score} points!',
                    style: Theme.of(context).textTheme.headlineSmall,
                  ),
                if (_gameState == GameState.win)
                  const SizedBox(height: 20),
                Row(
                  children: [
                    Expanded(
                      flex: 7,
                      child: TextField(
                        controller: _playerNameTextFieldController,
                        decoration: const InputDecoration(hintText:"Player name..."),
                        onSubmitted: (_) => _addUser(),
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: OutlinedButton(
                        onPressed: _isPlayerNameFilled ? _addUser : null,
                        child: const Text('Add player'),
                      ),
                    )

                  ],
                ),
                const SizedBox(height: 20),
                _players.isEmpty ? const Text('Please add the players') :
                Expanded(
                  child: ListView.builder(
                    itemCount: _players.length,
                    itemBuilder: (BuildContext context, index) {
                      final player = _players[index];
                      return Dismissible(
                          direction: DismissDirection.endToStart,
                          key: Key(player.name),
                          onDismissed: (direction) {
                            setState(() => _players.removeAt(index));
                            ScaffoldMessenger.of(context)
                              .showSnackBar(SnackBar(
                                content: Row(
                                  children: [
                                    const Icon(
                                      Icons.delete_outline,
                                      color: Colors.white,
                                    ),
                                    Text('${player.name} removed'),
                                  ]
                                )
                              )
                            );
                          },
                          background: Container(
                            margin: const EdgeInsets.symmetric(horizontal: 15),
                            alignment: Alignment.centerRight,
                            child: const Icon(Icons.delete_outline),
                          ),
                        child: ListTile(
                          leading: CircleAvatar(
                            backgroundColor: player.color,
                            foregroundColor: Colors.white,
                            child: Text(player.name.substring(0, 1))
                          ),
                          trailing: player.score > 0 ? Text('${player.score}') : null,
                          title: Text(player.name),
                        )
                      );
                    }
                  )
                )
              ],
            ) :
            Row(
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child:
                  ListView.builder(
                    itemCount: _players.length,
                    itemBuilder: (BuildContext context, index) {
                      final player = _players[index];
                      return PlayerListItem(player, index);
                    }
                  )
                ),
                Expanded(
                    flex: 8,
                    child: Column(
                      children: [
                        Text('${_players.first.name}: $_roundScore'),
                        InkWell(
                          onTap: _diceScore == 1 ? null : () {
                            final score = _randomGenerator.nextInt(6) + 1;
                            setState(() {
                              _diceScore = score;
                              _roundScore = score == 1 ? 0 : _roundScore + score;
                              if (_players.first.score + _roundScore >= _targetScore) {
                                _players.first.score += _roundScore;
                                _players.sort((a, b) => b.score.compareTo(a.score));
                                _gameState = GameState.win;
                              }
                            });
                          },
                          child: Dice(
                            value: _diceScore ?? 0,
                            backgroundColor: _players.first.color,
                          ),
                        ),
                        const SizedBox(height: 10),
                        OutlinedButton(
                          onPressed: _diceScore == null ? null : () => setState(() {
                            final item = _players.removeAt(0);
                            item.score += _roundScore;
                            _players.add(item);
                            _roundScore = 0;
                            _diceScore = null;
                          }),
                          child: const Text('Next Player'),
                        ),
                        Expanded(
                          child: Align(
                            alignment: Alignment.bottomCenter,
                            child: OutlinedButton(
                              onPressed: () => setState(() {
                                _players.forEach((p) => p.score = 0 );
                                _gameState = GameState.config;
                              }),
                              child: const Text('Give up'),
                            ),
                          )
                        )
                      ],
                    )
                  )
                ]
              ),
        )
    );
  }

  void _addUser() {
    setState(() {
      _players.add(Player(_playerNameTextFieldController.text, playerColor[_players.length % playerColor.length]));
      _playerNameTextFieldController.text = '';
    });
  }

  void _initGame() {
    _diceScore = null;
    _roundScore = 0;
    _players.forEach((p) => p.score = 0);
  }
}

// currently using a Badge around a CircleAvatar does not seem to work correctly
// https://github.com/flutter/flutter/issues/121444
class PlayerListItem extends StatelessWidget {
  const PlayerListItem(this.player, this.index, {super.key});
  final Player player;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
      Padding(
        padding: const EdgeInsets.fromLTRB(10, 10, 0 , 0),
        child: Badge.count(
          // alignment: AlignmentDirectional.topEnd,
          alignment: const AlignmentDirectional(25, 0),
          count: player.score,
          backgroundColor: Colors.black26,
          child: CircleAvatar(
            backgroundColor: player.color,
            foregroundColor: Colors.white,
            child: Text(player.name.substring(0, 1))
          ),
        ),
      ),
      ListTile(
        title: Text(player.name),
      )]
    );
  }
}

class Dice extends StatelessWidget {
  const Dice({super.key, required this.value, this.backgroundColor = Colors.red});
  final int value;
  final Color backgroundColor;

  // cc-by-sa julemand101 https://stackoverflow.com/a/67054466
  /// Returns true if the right most [bit] in the binary number [value] is 1
  bool isBitSet(int value, int bit) => (value & (1 << bit)) != 0;

  @override
  Widget build(BuildContext context) {
    // dart does not seem to have a good way to enter binary numbers
    const values = [
      16,  // 000 010 000
      257, // 100 010 001
      273, // 100 000 001
      325, // 101 000 101
      341, // 101 010 101
      455, // 111 000 111
    ];
    return UnconstrainedBox(child:
      SizedBox(
        width: MediaQuery.of(context).size.width * 0.20,
        height: MediaQuery.of(context).size.width * 0.20,
        child: Container(
          padding: const EdgeInsets.all(7),
          decoration: BoxDecoration(
            color: backgroundColor,
            borderRadius: const BorderRadius.all(Radius.circular(10)),
          ),
          child: value == 0 ?
          const FittedBox(
            fit: BoxFit.contain,
            // child:Text('?', style: TextStyle(color: Colors.white))
            // child:Text('Roll', style: TextStyle(color: Colors.white))
            // child:Icon(Icons.touch_app_outlined, color: Colors.white)
            child:Icon(Icons.pan_tool_alt_outlined, color: Colors.white)
          ) :
          GridView.count(
          crossAxisSpacing: 5.0,
          mainAxisSpacing: 5.0,
          padding: const EdgeInsets.all(5),
          shrinkWrap: true,
          crossAxisCount: 3,
          children: [for (var i = 0; i < 9; i++)
            isBitSet(values[value - 1], 8 - i) ? FractionallySizedBox(
              widthFactor: 0.8,
              heightFactor: 0.8,
              child: Container(
                decoration: const BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                ),
              )
            ) : Container()
          ],
        ))
      ),
    );
  }
}
